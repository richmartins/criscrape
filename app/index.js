const puppeteer = require('puppeteer');
const sendmail = require('sendmail')();

(async () => {
    const browser = await puppeteer.launch({
        args: [
        // Required for Docker version of Puppeteer
        '--no-sandbox',
        '--disable-setuid-sandbox',
        // This will write shared memory files into /tmp instead of /dev/shm,
        // because Docker’s default for /dev/shm is 64MB
        '--disable-dev-shm-usage'
    ]});
    const page = await browser.newPage();
    await page.goto('https://crisrv2.intranet.epfl.ch/fmi/webd/Vente_Materiel');
    await page.waitFor(5000);

    let bodyHTML = await page.evaluate(() => document.body.innerText);

    var re = new RegExp("désolés");
    
    if (re.test(bodyHTML)) {
        console.log("pas d'objet a vendre");
        
    } else {
        console.log("objet a vendre");
        sendmail({
            from: 'noreply@epfl.ch',
            to: 'nicolas.chevalley@epfl.ch, richard.martinstenorio@epfl.ch',
            subject: 'CRI Nouveau objet a vendre',
            html: 'nouveau objet a vendre sur le site du <a href="https://crisrv2.intranet.epfl.ch/fmi/webd/Vente_Materiel">cri</a><br /><br />E-mail envoyer via : nodejs::sendmail.js'
        }, function (err, reply) {
            console.log(err && err.stack)
            console.dir(reply)
        })
        
    }
    await browser.close();
})();